var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt   = require('bcrypt-nodejs');

var UserSchema = new Schema({
     username: {
         type      : String,
         unique    : true,
         required  : true,
         minlength : 3,
         maxlength : 30,
         index     : true
     },
     password: {
        type      : String,
        required  : true,
        minlength : 3,
        maxlength : 30
    },
     name: {
         type       : String,
         required   : true,
         minlength  : 3,
         maxlength  : 30
     },
     mobileno: {
         type      : Number,
         unique    : true,
         required  : true,
         minlength : 10,
        maxlength  : 10
     },
     email: {
        type      : String,
        unique    : true,
        required  : true,
        minlength : 11,
        maxlength : 30
     },
     role: {
         type      : String,
         required  : true,
         minlength : 3,
        maxlength  : 10
     }
});

UserSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) 
            {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});
UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', UserSchema);

