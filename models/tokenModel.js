var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var bcrypt   = require('bcrypt-nodejs');

var TokenSchema = new Schema({
    'token'     : String,
    'role'      : String,
    'username'  : String
});


module.exports = mongoose.model('Token', TokenSchema);

