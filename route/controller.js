const express = require('express');
const jwt     = require('jsonwebtoken');
const router  = express.Router();

const User      = require('../models/UserModel');
const Token     = require('../models/tokenModel');
const config    = require('../config/connection');

//============================= SIGNUP ========================================

router.post('/Signup', function(req, res) {

    if (!req.body.username || !req.body.password || !req.body.name || !req.body.mobileno || !req.body.email || !req.body.role) 
    {
        res.json({success: false, msg: 'Please Provide All Details'});
    } 
    else 
    {
        var newUser = new User({
          username : req.body.username,
          password : req.body.password,
          name     : req.body.name,
          mobileno : req.body.mobileno,
          email    : req.body.email,
          role     : req.body.role
        });
        
        newUser.save(function(err) {
            if(err) 
            {
              if(err._message == 'User validation failed')
              {

                return res.json({status: "Error", message: err.message});
              }
              else
              {
                return res.json({status: "Error", message: "User Already Exists"});
              }
            }
               res.json({status: "Success", message: 'Successfully created new user.'});
        });
    }
});
//============================= LOGIN ========================================
router.post('/Login', function(req, res) {
   if (!req.body.username || !req.body.password) 
   {
          res.json({success: false, msg: 'Please Provide username and password.'});
   }
   else
   {
              User.findOne({username: req.body.username},function(err, user) {
                if (err) throw err;
                if (!user) 
                {
                  res.status(401).send({status: "Error", message: 'Authentication failed. User not found.'});
                } 
                else
                {
             
                  user.comparePassword(req.body.password, function (err, isMatch) {
                        if (isMatch && !err) 
                        {
                            const token = jwt.sign(user.toJSON(), config.secret, {expiresIn: 1440});
                                
                                 var newToken = new Token({
                                    token    : token,
                                    role     : user.role,
                                    username : user.username
                                 });
                          
                                  newToken.save(function(err) {
                                      if(err) {console.log("token error")}
                                      else{console.log("token scucesssuccess")}
                                  });

                            res.json({success: true, token: token});
                        }
                        else 
                        {
                           res.status(401).send({status: "Error", message: 'Authentication failed. Wrong password.'});
                        }
                  });
                }
              });
   }
  
});

//============================= GET ========================================

router.get('/Get', function(req, res,next) {

  if(!req.body.token) 
  {
    return res.status(401).send({status: "Error", message: 'Authentication failed. Please Provide Access Token.'});
  }
  else
  {   
        Token.findOne({'token':req.body.token}, function(err, data) { 
          if(err)
          { 
              res.json({status: "Error", message: 'Please Provide Valid AccessToken'});

          }
          else
          {    
                  req.role     = data.role;
                  req.username = data.username;
                  next();
          }
      });
   }
});

router.use('/Get', function(req, res,next) {

   var role     = req.role;
   var username = req.username;

   if(role == 'admin')
   {
          User.find({}, function(err, data) {
              if (err)
              {
                    res.json({status: "Error", message: 'Record Not Found'});
              } 
              else
              {
                   res.json(data)
              }
          });
   }
   else
   {        
          User.findOne({'username':username}, function(err, data) {
              if (err)
              {
                    res.json({status: "Error", message: 'Record Not Found'});
              } 
              else
              {
                   res.json(data)
              }
          });
   }
   
});
//============================= EDIT ========================================

router.put('/Edit', function(req, res) {

    if (!req.body.token) 
    {
        res.json({success: false, msg: 'Please Provide Access Token'});
    } 
    else 
    {
        var newUser = new User({
          id       : "",
          username : req.body.username,
          password : req.body.password,
          name     : req.body.name,
          mobileno : req.body.mobileno,
          email    : req.body.email,
          role     : req.body.role
        });

           User.findOne({username: req.body.username},function(err, data) {

            if(err)
            {
                res.json({status: "Error", message: 'Updation Failed'});
            } 
            else
            {
              
                     data.username = req.body.username;
                     data.password = req.body.password;
                     data.name = req.body.name;
                     data.mobileno = req.body.mobileno;
                     data.email = req.body.email;
                     data.role = req.body.role;

                    User.findByIdAndUpdate({'_id': data.id},data,
                        {upsert: true, new: true, overwrite: true}, function(err) {
                    if(err) 
                    {
                     
                         res.json({status: "Error", message: 'Updation Failed'});
                    }
                    else
                    {

                       res.json({status: "Success", message: 'Successfully Updated User'});
                    }
                });
            }

           });
        
        
    }
});
//============================= DELETE ========================================

router.delete('/Delete', function(req, res,next) {

  if (!req.body.username || !req.body.token)
  {
         res.json({status: "Error", message: 'Please Provide Accesstokne and Username'});
  }
  else
  {
             Token.findOne({'token':req.body.token}, function(err, data) {
              if (err)
              {
                    res.json({status: "Error", message: 'Record Not Found'});
              } 
              else
              {     
                    req.inpUsertoken = req.body.token;
                    req.inpUsername  = req.body.username;
                    req.data         = data;
                    next();
              }
           });   
   }
});


router.use('/Delete', function(req, res,next) {

     var data        = req.data;
     var inpUsername = req.inpUsername;
     var inpToken    = req.inpUsertoken;
     var tempRole    = 'user';
    
     if((inpUsername == data.username) && (inpToken == data.token) && (tempRole == data.role))
     {
            User.deleteOne({"username":inpUsername}, function(err, data) {
                if (err)
                {
                      res.json({status: "Error", message: 'Record Not Found'});
                } 
                else
                {           
                          Token.deleteOne({"username":inpUsername}, function(err, data) {
                            if (err)
                            {
                                 console.log(err)
                            } 
                            else
                            {
                                  //res.json({status: "Success", message: req.body.username+"-"+'Deleted Successfully'});
                            }
                       });
                      res.json({status: "Success", message: req.body.username+"-"+'Deleted Successfully'});
                }
           });
     }
     else if((tempRole !== data.role) && (inpToken == data.token))
     {
            User.deleteOne({"username":req.body.username}, function(err, data) {
                if (err)
                {
                      res.json({status: "Error", message: 'Record Not Found'});
                } 
                else
                {
                            Token.deleteOne({"username":inpUsername}, function(err, data) {
                            if (err)
                            {
                                 console.log(err)
                            } 
                            else
                            {
                                  //res.json({status: "Success", message: req.body.username+"-"+'Deleted Successfully'});
                            }
                       });
                      res.json({status: "Success", message: req.body.username+"-"+'Deleted Successfully'});
                }
           });
     }
     else
     {
            res.json({status: "Error", message: 'You Are Not Access to Delete The Records'});
     }  

});

module.exports = router;

