
// requiring the express, mongoose and route
const express    = require('express');
const mongoose   = require('mongoose');
const app        = express();
var bodyParser   = require('body-parser');
const cors       = require('cors');
const route      = require('./route/controller');
var   config     = require('./config/connection');


mongoose.connect(config.database,{useCreateIndex: true,useNewUrlParser: true,useFindAndModify:false});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


//using the route 
app.use(route);

//declaring dynamic port or assign it 3000
const port = process.env.PORT || 3030;

//listening to the port
app.listen(port, ()=>{
    console.log("Listening... " +port);
});

